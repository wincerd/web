activate_this = '/home/e/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import os
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/FlaskApp/")
sys.path.append('/home/e/.local/lib/python3.6/site-packages/pip')

from FlaskApp import app as application

