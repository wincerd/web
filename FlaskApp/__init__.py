##from webui import WebUI # Add WebUI to your imports
import flask
from flask import Flask, render_template, flash, request, url_for, redirect, session, g,Response
from functools import wraps
from flask_wtf import Form
from wtforms import TextField, BooleanField, validators, PasswordField, Form
from passlib.hash import sha256_crypt
from MySQLdb import escape_string as thwart
from datetime import datetime 
from .accounts import Journal
import gc


import MySQLdb
import json

try:
    from .dbconnect import connection 
    c,conn = connection()
except:
    from .dbconnect import connect
    c,conn = connect()
    c.execute('''
        CREATE DATABASE  IF NOT EXISTS pos;
    ''')

# TOPIC_DICT = Content()
app = Flask(__name__)
##ui = WebUI(app,debug=True) # Create a WebUI instance


#initialize the accounting module 
newj =Journal()

app.secret_key = 'my unobvious secret key'

@app.route('/home')
def homepage():
    " Dashboard "
    return render_template("home.html")
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('you need to login first')
            return render_template('login.html')
        
    return wrap
@app.route('/')
@app.route('/<Time>/', methods=["POST", "GET"])
@login_required
def dashboard(Time=None):
    c,conn  = connection()
    acounts = c.execute('select * from Account')
    if acounts == 0:
        flash("creating initiall  accounts")
        newj.init_acc()
    if Time == "week":
        days= newj.week()
        data=[]
        for day in days:
            day= day.strftime("%Y%m%d")
            amount=newj.get_account_balance("cash",day,day)
            data.append(amount)
        labels = ["monday","tuesday","wednesday","thursday","friday","sartaday","sunday"]
        return render_template('main.html', data=data, labels=labels)
    elif Time == "month":
        days,numbers = newj.month()
        label=[]
        data = []
        for i in numbers:
            label.append(int(i))
        for day in days:
            day=(day).strftime("%Y%m%d")
            data.append(newj.get_account_balance("cash",day,day))
        labels=[]
        for i in range(1,len(label)+1):
            labels.append(i)
        return render_template('main.html', data=data, labels=labels)
    elif Time == "year":
        labels = ["January","February","March","April","May","June","July","August","september","november","december"]
        data= []
        for i in range(1,13):
            monts=[]
            dates,m= newj.month(i)
            for day in dates:
                day=(day).strftime("%Y%m%d")
                monts.append(newj.get_account_balance("cash",day,day))
            print(monts)
            data.append(sum(monts))
            print(data)
        return render_template('main.html', data=data, labels=labels)
    elif Time== None:
        labels = ["January","February","March","April","May","June","July","August"]
        data = [10,9,8,7,6,4,7,8]
        return render_template('main.html', data=data, labels=labels)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')
@app.errorhandler(405)
def method_not_found(e):
    return render_template('405.html')

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [validators.Required(),
                                              validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the Terms of Service and Privacy Notice (updated Jan 01,2019)',
                              [validators.Required()])

@app.route('/login/',  methods = ["GET","POST"])
def login_page():
    c,conn  = connection()
    error = ''
    try:
        if request.method == 'POST':
            c.execute('SELECT * FROM users WHERE username = (%s)',(thwart(request.form['username']),))
            data = c.fetchone()[1]
            if sha256_crypt.verify(request.form['password'], data) :
                session['logged_in'] = True
                session['username'] = request.form['username']
                flash('You are now logged in')
                return redirect(url_for('dashboard'))
            else:
                error = 'Invalid credentials check Your Password, try again.'
        gc.collect()
        return render_template('login.html', error=error)
    except Exception as e:
        error = 'Invalid credentials, Check your username try again.'
        return render_template('login.html', error=error)
@app.route('/logout')
@login_required
def logout():

    session.clear()
    flash('You have been logged out!')
    gc.collect()
    return redirect(url_for('homepage'))
# @app.route('/buy/', methods = ["GET","POST"])
# @login_required
# def buy_products():
#     c,conn  = connection()
#     if request.method == "POST":
#         #get form data
#         product =form.product.data
#         cost=form.cost2.data
#         amount = form.amounts.data
#         #check if product exists
#         c.execute("select * from products where name = %s ",(product))
#         name = c.fetchone()
#         if name == None:
#             #save the product / service as new
#             product_num = newj.get_lastproductid()
#             desctiption =('buying {}'.format(name))
#             c.execute("INSERT INTO products (name, product_num,size,category,description,cog,price,account) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", 
#             (name,product_num,amount,category,description,cost,price,account))
#             #add new record in the journal
#             accounts= ("cash,inventory")
#             price= 
#             newj.record_transaction(accounts,price)
#     else:
#         return redirect(url_for('products'))
    

@app.route('/register/', methods=["GET", "POST"])
def register_page():
    c,conn  = connection()
    try:
        form = RegistrationForm(request.form)
        if request.method == "POST" and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.encrypt((str(form.password.data)))
            # c, conn = connection()
            # x = c.execute("SELECT * FROM users WHERE username = %s",(thwart(username)),)
            x = c.execute("""SELECT * FROM users WHERE username  =  %s""", (thwart(username),))
            if int(x) > 0:
                flash("That username is already taken, please choose another")
                return render_template('register.html', form=form)
            else:
                c.execute("""INSERT INTO users (username, userpass, email) VALUES (%s, %s, %s)""",
                          (thwart(username), thwart(password), thwart(email)))
                conn.commit()
                flash("Thanks for registering!")
                c.close()
                conn.close()
                gc.collect()
                session['logged_in'] = True
                session['username'] = username
                return redirect(url_for('dashboard'))
        return render_template("register.html", form=form)

    except Exception as e:
        return (str(e))
@app.route('/inventory', methods= ['POST'])
@login_required
def inventory():
    c,conn  = connection()
    if request.method == 'POST':
        #get form data
        name = request.form ['name']
        productid = request.form['productid']
        description = request.form["desc"]
        cog = request.form['cost']
        price= request.form['price']
        account = request.form['account']
        category = request.form['category'] 
        size = request.form['size']
        accounts = ("cash","inventory")
        #get the product id
        c.execute("select product_num from products where product_num =( SELECT MAX(product_num) FROM products)")
        productid =(sum(c.fetchone()) + 1)
        #add new record in the journal
        newj.record_transaction(accounts,price)
        #add product details to the products 
        c.execute("INSERT INTO products (name, product_num,size,category,description,cog,price,account) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", 
        (name,productid,size,category,description,cog,price,account))
        conn.commit()
        conn.close()
        return render_template("sale.html")
    else:
        return render_template("sale.html")

@app.route('/products/')
@login_required
def  products():
    c,conn  = connection()
    query = "SELECT * from products"
    c.execute(query)
    data = c.fetchall()
    if data ==():
            flash("please add products to continue")
    conn.close()
    return render_template("products.html",data=data)
@app.route('/contacts/')
@app.route('/contacts/<cont>/', methods=["POST", "GET"])
@login_required
def contacts(cont = None):
    c,conn  = connection()
    #check if the data needed is for the suppliers
    if cont == 'suppliers':
        c.execute("select * from contact where type = %s""", ('supplier',))
        data = c.fetchall()
        if data ==():
            flash("please add supplier contacts to continue")
        return render_template("contacts.html",data = data)
    #check if the data needed is for the customers
    elif cont == 'customers':
        c.execute("""select * from contact where type =%s""", ('customers',))
        data = c.fetchall() 
        if data ==():
            flash("please add supplier contacts to continue")
        return render_template("contacts.html",data = data)
    #save new supplier /customer
    if request.method == 'POST':
        name = request.form ['name']
        typ = cont
        number = request.form["number"]
        c_id =request.form["email"]
        #add the records in the data base
        c.execute("INSERT INTO contact (name,type,number,c_id) VALUES (%s,%s,%s,%s)", 
        (name,typ,number,c_id))
        conn.commit()
        conn.close()
        return render_template("contacts.html",data = data)
    c.execute("select * from contact")
    data = c.fetchall()
    return render_template("contacts.html",data = data)
@app.route('/savy/', methods=['GET', 'POST'])
@login_required
def sv_page():
    if request.method == 'POST':
        name = request.form ['name']
        typ = request.form ['type']
        ID  = request.form ['ID']
        #add the account to the accounts table
        newj.create_account(ID,name,typ)
        flash('account {}of type {} is created'.format(name,typ))
        return render_template('sales.html')


@app.route('/sale/', methods=['GET', 'POST'])
@login_required
def sales_page():
    c,conn  = connection()
    #cash/sales
    if request.method == 'POST':
        #get the data as json data type from javascript
        data = request.get_json()
        #check for empty  rows
        for row in data:
            save = [] 
            for cell  in row:
                if row[cell] == "":
                    pass
                else:
                    save.append(row[cell])     
                    #save the table data if all rows exists 
                    if len(save) > 4:
                        amount= save[4]
                        account = ("cash","sale")
                        #get the cost of the item
                        name = save[1]
                        c.execute("""select cog from products where name =%s""", (name,))
                        cog = c.fetchone()
                        if cog == ():
                            flash("please add product first or buy")
                            cog == save[4]
                        #enter the record into the ledger
                        newj.record_transaction(account,amount)
                        Date = (newj.Date()).strftime("%Y%m%d")
                        #get the sale id
                        c.execute("select ID from sale where ID =( SELECT MAX(ID) FROM sale)")
                        sale_id =(sum(c.fetchone()) + 1)
                        c.execute("""INSERT INTO sale(ID,item,Date,description,size,amount,cog) VALUES (%s,%s,%s,%s,%s,%s,%s)""", 
                        (sale_id,save[1],Date,save[2],save[3],save[4],cog,))
                        conn.commit()
                        conn.close()
                        flash("successfully made a sale")
                        return redirect(url_for('sales_page'))
    return render_template('sale2.html')
@app.route('/expense/',methods=['GET', 'POST'])
@app.route('/expense/<account>')
@login_required
def add_expense(account= "utilities"):
    if  request.method == 'POST':
        amount =request.form['amount']
        description= request.form['description']
        accounts=(account,'cash')
        newj.record_transaction(account,amount)
        flash("recorded successfully")
        return render_template('expenses.html',account = account)
    return render_template('expenses.html',account = account)
@app.route('/addacc/', methods=["GET", "POST"])
@login_required
def add_account():
    if request.method == 'POST':
        #get form data from the view
        name = request.form ['name']
        typ = request.form ['type']
        ID  = request.form ['ID']
        #add the account to the accounts table
        new_acc = newj.create_account(ID,name,typ)
        flash('account {}of type {} is created'.format(name,typ))
        return render_template('account.html')
@app.route('/account/')
@login_required
def accounts():
    return render_template('account.html')
@app.route('/reports/')
@app.route('/reports/<report>')
@login_required
def reports(report="balancesh"):
    if  report == "balancesh":
        data = newj.get_balance_sheet()
        return render_template('balance_sheet.html',data = data)
    elif report == "profitL":
        data = newj.get_income_statement()
        return render_template('profit.html',data = data)
    else:
        return '', 400
class SearchForm(Form):
    autocomp = TextField('Insert City', id='city_autocomplete')

@app.route('/_autocomplete', methods=['GET'])
def autocomplete():
    c,conn  = connection()
    if request.method ==("GET"):
        values=[]
        c.execute("select name from products")
        data = c.fetchall()
        for items in data:
            for i in items:
                values.append(i)
    # values = ["Bratislava",
    #   "Banská Bystrica",
    #   "Prešov",
    #   "Považská Bystrica",
    #   "Žilina",
    #   "Košice",
    #   "Ružomberok",
    #   "Zvolen",
    #   "Poprad"]
    return Response(json.dumps(values), mimetype='application/json')
@app.route('/test/',methods= ["POST","GET"])
def test():
    form = SearchForm(request.form)
    return render_template('test.html',form = form)
if __name__ == '__main__':
    app.run() #replace app.run() with ui.run(), and that's it
