class ClientConfig(object):
    PUBLIC_KEY = 'None'
    APP_NAME = 'app'
    COMPANY_NAME = 'mine'
    UPDATE_URLS = ['http://auta.pythonanywhere.com/version/']
    MAX_DOWNLOAD_RETRIES = 3
